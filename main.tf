terraform {
  required_providers {
    random = {
      source = "hashicorp/random"
    }
  }
  required_version = ">= 0.13"
}

variable "name" {
  default = "David"
}

resource "random_pet" "pet" {
  keepers = {
    uuid = uuid()
  }
}

output "hello_name" {
  value = "Hello, ${var.name}"
}

output "random_pet" {
  value = random_pet.pet.id
}

