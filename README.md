# Initial Test

Basic Terraform template for testing run triggering from UI and VCS commits.  Uses basic Terraform constructs, so does not require any cloud or platform credentials.

## Displays Name Var and Random Pet

On each run, the following items are displayed

- "Hello, USERNAME" message
  - USERNAME is set by Terraform variable name (default = David)
  - Output will remain consistent between applies
    - Unless, the Terraform variable "name" is changed
- The name of a randomly generated Pet name
  - This value will change on every apply
